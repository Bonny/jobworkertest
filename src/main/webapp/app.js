/* global _APP_INFO_, _DEBUG_ */

(function () {
    'use strict';

    var app = angular.module('app', [

    ]);

    app.run(['$log',
        function ($log) {
            $log.info("App is started");
        }]);

//    app.config([
//        function () {
//        }
//    ]);

    /**
     chronology: {
     id: "ISO", calendarType: "iso8601"
     }
     calendarType: "iso8601"
     id: "ISO"
     dayOfMonth: 12
     dayOfWeek: "WEDNESDAY"
     dayOfYear: 346
     hour: 13
     minute: 36
     month: "DECEMBER"
     monthValue: 12
     nano: 700000000
     second: 34
     year: 2018
     */
    app.directive("localDateTime", ['$log', function ($log) {
            return {
                restrict: 'AE',
                transclude: true,
                template: '<span>{{r.dd}}/{{r.mm}}/{{r.yy}} {{r.h}}:{{r.m}}</span>',
                scope: {
                    input: "="
                },
                link: function (scope, element, attrs) {
                    scope.r = {dd: "--", mm: "--", yy: "----", h: "--", m: "--"};
                    if (scope.input) {
                        var i = scope.input;
                        scope.r.dd = i.date.day;
                        scope.r.mm = i.date.month;
                        scope.r.yy = i.date.year;
                        scope.r.h = i.time.hour;
                        scope.r.m = i.time.minute;
                        for (var j in scope.r) {
                            if (scope.r[j] < 10) {
                                scope.r[j] = "0" + scope.r[j];
                            }
                        }
                    }
                }
            };
        }]);

    app.controller('MainCtrl', ['$scope', '$log', '$http',
        function ($scope, $log, $http) {
            $log.debug("MainCtrl.load");

            $scope.total = 0;
            $scope.items = [];
            $scope.loading = false;
            $scope.error = false;
            $scope.errorMsg = '';

            var error = function (res) {
                $log.debug("MainCtrl error");
                var ex = res ? angular.extend({code: "UNKNOW", reason: "unknow-error", status: 500}, res) : ex;
                $scope.loading = false;
                $scope.error = true;
                $scope.errorMsg = ex.reason;
            };

            var success = function (res) {
                $log.debug("MainCtrl success");
                $scope.loading = false;
                $scope.items = res.items;
                $scope.total = res.total;
            };

            $scope.refresh = function () {
                $scope.loading = true;
                $scope.error = false;
                $scope.items = [];
                $scope.total = 0;
                $http.get("./api/event").success(success).error(error);
            };

            $scope.addEvent = function () {
                $http.post("./api/event").success(function (res) {
                }).error(function (res) {
                });
            };


            $scope.download = function (item) {
                $log.debug("download: " + item.eventId);
                var a = document.createElement('a');
                a.href = "./api/event/download/" + item.eventId;
                a.target = '_blank';
                a.click();
            };


            $scope.refresh();

        }]);
})();
