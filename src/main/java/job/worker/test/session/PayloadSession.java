package job.worker.test.session;

import it.job.worker.beans.JobEvent;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.apache.log4j.Logger;
import it.job.worker.JobSession;
import java.io.PrintWriter;

@Stateless
@LocalBean
public class PayloadSession implements JobSession {

    final Logger LOGGER = Logger.getLogger(PayloadSession.class);

    @Override
    public void execute(JobEvent evt) throws Exception {
        LOGGER.info("START");
        PrintWriter writer = new PrintWriter(evt.getOutFilePath(), "UTF-8");
        writer.println("The first line");
        writer.println("The second line");
        writer.close();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            LOGGER.error(ex);
        }
        LOGGER.info("STOP");
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
