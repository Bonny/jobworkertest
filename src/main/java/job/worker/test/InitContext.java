package job.worker.test;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import org.apache.log4j.BasicConfigurator;

@WebListener
public class InitContext implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        BasicConfigurator.configure();
        System.out.println("Start JobWorkerTest");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("Stop JobWorkerTest");
    }
}
