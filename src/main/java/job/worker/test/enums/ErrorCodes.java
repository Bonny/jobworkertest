package job.worker.test.enums;

import javax.ws.rs.core.Response;

/**
 *
 * @author luca
 */
public enum ErrorCodes {

    UNKNOW(500, "unknow-error"),
    BAD_REQUEST(Response.Status.BAD_REQUEST.getStatusCode(), "input-not-invalid"),
    NOT_FOUND(Response.Status.NOT_FOUND.getStatusCode(), "not-found"),
    ///// AUTH ERROR

    /**
     * {@value} = auth-invalid-credential
     */
    EA00(Response.Status.BAD_REQUEST.getStatusCode(), "auth-invalid-credential"),
    /**
     * {@value} = auth-profile-suspended
     */
    EA01(Response.Status.FORBIDDEN.getStatusCode(), "auth-profile-suspended"),
    /**
     * {@value} = auth-not-authorized
     */
    EA02(Response.Status.FORBIDDEN.getStatusCode(), "auth-not-authorized"),
    ///// TOKEN ERROR

    ET00(500, "token-build"),//build error
    ET01(Response.Status.UNAUTHORIZED.getStatusCode(), "token-expired"),//expired
    ET02(Response.Status.UNAUTHORIZED.getStatusCode(), "token-illegal-argument"),//illegal args
    ET03(Response.Status.UNAUTHORIZED.getStatusCode(), "token-malformed"),//malformed
    ET04(Response.Status.UNAUTHORIZED.getStatusCode(), "token-signature");//signature

    private String reason;
    private int status;

    private ErrorCodes(int status, String reason) {
        this.status = status;
        this.reason = reason;
    }

    private ErrorCodes(String reason) {
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }

    public int getStatus() {
        return status;
    }

    public String toJson() {
        return new StringBuilder("{ ")
                .append(String.format("\"code\" : \"%s\"", name()))
                .append(", ")
                .append(String.format("\"reason\" : \"%s\"", getReason()))
                .append(", ")
                .append(String.format("\"status\" : %d", getStatus()))
                .append(" }")
                .toString();
    }

}
