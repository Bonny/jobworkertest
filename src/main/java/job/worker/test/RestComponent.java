package job.worker.test;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import org.apache.log4j.Logger;

/**
 *
 * @author luca
 */
public abstract class RestComponent {

    Logger logger = Logger.getLogger(getClass());

    @Context
    ResourceInfo resourceInfo;

    @Context
    HttpServletRequest request;

    public String getLogPrefix() {
        StringBuilder sb = new StringBuilder();
        sb.append("[sUID=").append(getSessionID()).append("] ");
        return sb.toString();
    }

    protected Logger getLogger() {
        return logger;
    }

    protected ResourceInfo getResourceInfo() {
        return resourceInfo;
    }

    protected HttpServletRequest getRequest() {
        return request;
    }

    protected String getSessionID() {
        return getRequest().getSession().getId();
    }
}
