package job.worker.test.exception;

import job.worker.test.enums.ErrorCodes;
import javax.ws.rs.core.Response;

/**
 *
 * @author luca
 */
public abstract class AppException extends RuntimeException {
    
    ErrorCodes err = ErrorCodes.UNKNOW;
 
    public AppException() {
        super();
        err = ErrorCodes.UNKNOW;
    }
    
    public AppException(ErrorCodes err) {
        super();
        this.err = err;
    }
    
    public AppException(ErrorCodes err, Throwable cause) {
        super(cause);
        this.err = err;
    }
    
    public AppException(Throwable cause) {
        super(cause);
    }
    
    public ErrorCodes getErr() {
        return err;
    }
    
    @Override
    public String toString() {
        return getErr().toJson();
    }
    
    @Override
    public String getMessage() {
        return "[" + getErr().toJson() + "] " + super.getMessage();
    }
    
    public final static Response buildResponse(Exception ex) {
        return buildResponse(ex, null);
    }
    
    public final static Response buildResponse(ErrorCodes ec) {
        return Response.status(ec.getStatus()).entity(ec.toJson()).build();
    }
       
    public final static Response buildResponse(Exception ex, Object data) {
        return buildResponse(parseCode(ex));
    }
    
    public final static ErrorCodes parseCode(Exception ex) {
        return (ex != null && AppException.class.isAssignableFrom(ex.getClass()))
                ? ((AppException) ex).getErr()
                : ErrorCodes.UNKNOW;
    }

}
