package job.worker.test.exception;

import job.worker.test.enums.ErrorCodes;

/**
 *
 * @author kl0339
 */
public class RestException extends AppException {

    public RestException() {
    }

    public RestException(ErrorCodes err) {
        super(err);
    }

    public RestException(ErrorCodes err, Throwable cause) {
        super(err, cause);
    }

    public RestException(Throwable cause) {
        super(cause);
    }
    
}
