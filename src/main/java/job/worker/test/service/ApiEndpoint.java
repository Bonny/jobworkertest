package job.worker.test.service;

import com.google.gson.Gson;
import it.job.worker.beans.JobEvent;
import it.job.worker.beans.JobUtils;
import java.io.File;
import java.util.List;
import job.worker.test.RestComponent;
import job.worker.test.exception.AppException;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/event")
@RequestScoped
public class ApiEndpoint extends RestComponent {

    final String WORK_DIR = "C:\\Users\\kl0339\\output\\job\\";
    final String OUT_EXT = ".txt";

    @Inject
    Event<JobEvent> jobEvents;

    @Inject
    JobUtils jobUtils;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response addEvent() {
        try {
            getLogger().info(getLogPrefix() + "Call add event");

            JobEvent job = new JobEvent();
            job.addToPayload("TEST", "TEST1");

            job.setJobSession("java:module/PayloadSession");
            job.setEventName("TEST");
            job.setWorkDir(WORK_DIR);
            job.setLogPrefix(getLogPrefix());
            job.setOutExt(OUT_EXT);

            getLogger().info(getLogPrefix() + "START FIRE JOB");
            getLogger().info(getLogPrefix() + job);
            jobEvents.fire(job);
            getLogger().info(getLogPrefix() + "END FIRE JOB");

            return Response.ok().build();
        } catch (Exception ex) {
            return AppException.buildResponse(ex);
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStatus() {
        try {

            Result r = new Result();
            r.items = jobUtils.getList(WORK_DIR);
            r.total = r.items.size();

            return Response.ok(new Gson().toJson(r)).build();

        } catch (Exception ex) {
            return AppException.buildResponse(ex);
        }
    }
    
    @GET
    @Path("/download/{eventId}")
    public Response download(@PathParam("eventId") String eventId) {
        final String fileName = eventId + OUT_EXT;
        final String path = WORK_DIR + fileName;
        getLogger().debug(getLogPrefix() + "Download: " + path);
        Response.ResponseBuilder response = Response.ok((Object) new File(path));
        response.header("Content-Disposition", String.format("attachment; filename=\"%s\"", fileName));
        return response.build();
    }

    class Result {

        List<JobEvent> items;
        int total = 0;
    }
}
